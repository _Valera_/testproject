package ru.test.exception;


public class BankIdNotFoundException extends Exception {

    public BankIdNotFoundException(String message) {
        super(message);
    }
}
