package ru.test.exception;

public class AgentIdNotFoundException extends Exception {

    public AgentIdNotFoundException(String message) {
        super(message);
    }

}
