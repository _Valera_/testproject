package ru.test.service;

import ru.test.exception.AgentIdNotFoundException;
import ru.test.exception.BankIdNotFoundException;
import ru.test.model.BankId;
import ru.test.model.ProviderBankInfo;

public interface ProviderBankInfoService {

    ProviderBankInfo findById(Long id, Long bankId) throws AgentIdNotFoundException,BankIdNotFoundException;
    ProviderBankInfo findByBankId(Long id, BankId bankId) throws AgentIdNotFoundException,BankIdNotFoundException;
}
