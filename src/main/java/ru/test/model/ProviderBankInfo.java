package ru.test.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ProviderBankInfo {

    @JsonIgnore
    private Long idAgent;
    private ProviderInfo operatorInfo;
    private BankBaseInfo bankInfo;

    public Long getIdAgent() {
        return idAgent;
    }

    public void setIdAgent(Long idAgent) {
        this.idAgent = idAgent;
    }

    public ProviderInfo getOperatorInfo() {
        return operatorInfo;
    }

    public void setOperatorInfo(ProviderInfo operatorInfo) {
        this.operatorInfo = operatorInfo;
    }

    public BankBaseInfo getBankInfo() {
        return bankInfo;
    }

    public void setBankInfo(BankBaseInfo bankInfo) {
        this.bankInfo = bankInfo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProviderBankInfo that = (ProviderBankInfo) o;

        if (!idAgent.equals(that.idAgent)) return false;
        if (!operatorInfo.equals(that.operatorInfo)) return false;
        return bankInfo != null ? bankInfo.equals(that.bankInfo) : that.bankInfo == null;

    }

    @Override
    public int hashCode() {
        int result = idAgent.hashCode();
        result = 31 * result + operatorInfo.hashCode();
        result = 31 * result + (bankInfo != null ? bankInfo.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ProviderBankInfo{" +
                "idAgent=" + idAgent +
                ", operatorInfo=" + operatorInfo +
                ", bankInfo=" + bankInfo +
                '}';
    }
}
