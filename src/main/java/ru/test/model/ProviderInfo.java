package ru.test.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ProviderInfo {

    @JsonIgnore
    private Long idProviderInfo;
    private String recipientAccount;
    private String inn;
    private String kpp;

    public Long getIdProviderInfo() {
        return idProviderInfo;
    }

    public void setIdProviderInfo(Long idProviderInfo) {
        this.idProviderInfo = idProviderInfo;
    }

    public String getRecipientAccount() {
        return recipientAccount;
    }

    public void setRecipientAccount(String recipientAccount) {
        this.recipientAccount = recipientAccount;
    }

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public String getKpp() {
        return kpp;
    }

    public void setKpp(String kpp) {
        this.kpp = kpp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProviderInfo that = (ProviderInfo) o;

        if (!idProviderInfo.equals(that.idProviderInfo)) return false;
        if (!recipientAccount.equals(that.recipientAccount)) return false;
        if (!inn.equals(that.inn)) return false;
        return kpp.equals(that.kpp);

    }

    @Override
    public int hashCode() {
        int result = idProviderInfo.hashCode();
        result = 31 * result + recipientAccount.hashCode();
        result = 31 * result + inn.hashCode();
        result = 31 * result + kpp.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "ProviderInfo{" +
                "idProviderInfo=" + idProviderInfo +
                ", recipientAccount='" + recipientAccount + '\'' +
                ", inn='" + inn + '\'' +
                ", kpp='" + kpp + '\'' +
                '}';
    }
}
