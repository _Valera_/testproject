package ru.test.model;

public class BankBaseInfo {

    private Long idBankBaseInfo;
    private String name;
    private String correspondentAccount;
    private String address;
    private String inn;
    private String bik;
    private Boolean isClosed;

    public Long getIdBankBaseInfo() {
        return idBankBaseInfo;
    }

    public void setIdBankBaseInfo(Long idBankBaseInfo) {
        this.idBankBaseInfo = idBankBaseInfo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCorrespondentAccount() {
        return correspondentAccount;
    }

    public void setCorrespondentAccount(String correspondentAccount) {
        this.correspondentAccount = correspondentAccount;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public String getBik() {
        return bik;
    }

    public void setBik(String bik) {
        this.bik = bik;
    }

    public boolean isIsClosed() {
        return isClosed;
    }

    public void setClosed(boolean isClosed) {
        this.isClosed = isClosed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BankBaseInfo that = (BankBaseInfo) o;

        if (isClosed != that.isClosed) return false;
        if (!idBankBaseInfo.equals(that.idBankBaseInfo)) return false;
        if (!name.equals(that.name)) return false;
        if (!correspondentAccount.equals(that.correspondentAccount)) return false;
        if (!address.equals(that.address)) return false;
        if (!inn.equals(that.inn)) return false;
        return bik.equals(that.bik);

    }

    @Override
    public int hashCode() {
        int result = idBankBaseInfo.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + correspondentAccount.hashCode();
        result = 31 * result + address.hashCode();
        result = 31 * result + inn.hashCode();
        result = 31 * result + bik.hashCode();
        result = 31 * result + (isClosed ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "BankBaseInfo{" +
                "idBankBaseInfo=" + idBankBaseInfo +
                ", name='" + name + '\'' +
                ", correspondentAccount='" + correspondentAccount + '\'' +
                ", address='" + address + '\'' +
                ", inn='" + inn + '\'' +
                ", bik='" + bik + '\'' +
                ", isClosed=" + isClosed +
                '}';
    }
}
