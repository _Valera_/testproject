package ru.test.model;

public class BankId {

    private Long bankId;

    public Long getBankId() {
        return bankId;
    }

    public void setBankId(Long bankId) {
        this.bankId = bankId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BankId bankId1 = (BankId) o;

        return bankId.equals(bankId1.bankId);

    }

    @Override
    public int hashCode() {
        return bankId.hashCode();
    }

    @Override
    public String toString() {
        return "BankId{" +
                "bankId=" + bankId +
                '}';
    }
}
