package ru.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.test.exception.AgentIdNotFoundException;
import ru.test.exception.BankIdNotFoundException;
import ru.test.model.BankId;
import ru.test.model.ProviderBankInfo;
import ru.test.service.ProviderBankInfoService;

@Controller
public class ProviderBankInfoController {

    @Autowired
    ProviderBankInfoService providerBankInfoService;

    @RequestMapping(value = {"/agents/{agentId}/providerBankInfo"}, method = RequestMethod.GET)
    @ResponseBody
    public ProviderBankInfo getProviderBankInfo(@PathVariable("agentId") Long agentId,
                                                @RequestParam(value = "bankId", required = false) Long bankId
    ) throws AgentIdNotFoundException, BankIdNotFoundException {
        ProviderBankInfo providerBankInfoModel = providerBankInfoService.findById(agentId, bankId);
        return providerBankInfoModel;
    }

    @RequestMapping(value = {"/agents/{agentId}/providerBankInfo/search"}, method = RequestMethod.POST)
    @ResponseBody
    public ProviderBankInfo searchProviderBankInfo(@PathVariable("agentId") Long agentId,
                                                   @RequestBody BankId bankId
    ) throws AgentIdNotFoundException, BankIdNotFoundException {
        ProviderBankInfo providerBankInfoModel = providerBankInfoService.findByBankId(agentId, bankId);
        return providerBankInfoModel;
    }

}
