package ru.test.model.builder;

import ru.test.model.BankId;

public class BankIdBuilder {
    private BankId bank;

    public BankIdBuilder() {
        bank = new BankId();
    }

    public BankIdBuilder bankId(Long bankId) {
        bank.setBankId(bankId);
        return this;
    }

    public BankId build() {
        return bank;
    }
}
