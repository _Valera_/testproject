package ru.test.model.builder;

import ru.test.model.BankBaseInfo;

public class BankBaseInfoBuilder {

    private BankBaseInfo bankBaseInfo;

    public BankBaseInfoBuilder() {
        bankBaseInfo = new BankBaseInfo();
    }

    public BankBaseInfoBuilder idBankBaseInfo(Long idBankBaseInfo) {
        bankBaseInfo.setIdBankBaseInfo(idBankBaseInfo);
        return this;
    }

    public BankBaseInfoBuilder name(String name) {
        bankBaseInfo.setName(name);
        return this;
    }

    public BankBaseInfoBuilder correspondentAccount(String correspondentAccount) {
        bankBaseInfo.setCorrespondentAccount(correspondentAccount);
        return this;
    }

    public BankBaseInfoBuilder address(String address) {
        bankBaseInfo.setAddress(address);
        return this;
    }

    public BankBaseInfoBuilder inn(String inn) {
        bankBaseInfo.setInn(inn);
        return this;
    }

    public BankBaseInfoBuilder bik(String bik) {
        bankBaseInfo.setBik(bik);
        return this;
    }

    public BankBaseInfoBuilder isClosed(Boolean isClosed) {
        bankBaseInfo.setClosed(isClosed);
        return this;
    }

    public BankBaseInfo build() {
        return bankBaseInfo;
    }
}

