package ru.test.model.builder;

import ru.test.model.ProviderInfo;

public class ProviderInfoBuilder {
    private ProviderInfo providerInfo;

    public ProviderInfoBuilder() {
        providerInfo = new ProviderInfo();
    }

    public ProviderInfoBuilder idProviderInfo(Long idProviderInfo) {
        providerInfo.setIdProviderInfo(idProviderInfo);
        return this;
    }

    public ProviderInfoBuilder recipientAccount(String recipientAccount) {
        providerInfo.setRecipientAccount(recipientAccount);
        return this;
    }

    public ProviderInfoBuilder inn(String inn) {
        providerInfo.setInn(inn);
        return this;
    }

    public ProviderInfoBuilder kpp(String kpp) {
        providerInfo.setKpp(kpp);
        return this;
    }

    public ProviderInfo build() {
        return providerInfo;
    }
}