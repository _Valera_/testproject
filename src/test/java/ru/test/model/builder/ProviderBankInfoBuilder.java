package ru.test.model.builder;

import ru.test.model.BankBaseInfo;
import ru.test.model.ProviderBankInfo;
import ru.test.model.ProviderInfo;

public class ProviderBankInfoBuilder {

    private ProviderBankInfo providerBankInfo;

    public ProviderBankInfoBuilder() {
        providerBankInfo = new ProviderBankInfo();
    }

    public ProviderBankInfoBuilder idAgent(Long idAgent) {
        providerBankInfo.setIdAgent(idAgent);
        return this;
    }

    public ProviderBankInfoBuilder operatorInfo(ProviderInfo operatorInfo) {
        providerBankInfo.setOperatorInfo(operatorInfo);
        return this;
    }

    public ProviderBankInfoBuilder bankInfo(BankBaseInfo bankInfo) {
        providerBankInfo.setBankInfo(bankInfo);
        return this;
    }

    public ProviderBankInfo build() {
        return providerBankInfo;
    }
}

