package ru.test.configuration;


import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.test.service.ProviderBankInfoService;

@Configuration
public class TestContext {

    @Bean
    public ProviderBankInfoService providerBankInfoService() {
        return Mockito.mock(ProviderBankInfoService.class);
    }
}
