package ru.test.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.test.configuration.*;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static ru.test.util.UtilController.convertObjectToJsonBytes;

import ru.test.exception.AgentIdNotFoundException;
import ru.test.exception.BankIdNotFoundException;
import ru.test.model.BankBaseInfo;
import ru.test.model.BankId;
import ru.test.model.ProviderBankInfo;
import ru.test.model.ProviderInfo;
import ru.test.model.builder.BankBaseInfoBuilder;
import ru.test.model.builder.BankIdBuilder;
import ru.test.model.builder.ProviderBankInfoBuilder;
import ru.test.model.builder.ProviderInfoBuilder;
import ru.test.service.ProviderBankInfoService;
import ru.test.util.StaticVariable;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestContext.class, WebMvcConfiguration.class})
@WebAppConfiguration
public class ProviderBankInfoControllerTest {

    private static final Long AGENT_ID = StaticVariable.AGENT_ID;
    private static final Long BANK_ID = StaticVariable.BANK_ID;

    private MockMvc mockMvc;

    @Autowired
    private ProviderBankInfoService providerBankInfoServiceMock;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        Mockito.reset(providerBankInfoServiceMock);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

//////////////////POST
    @Test//2+3+
    public void findProviderBankInfoWithAgentIdExistWithBankIdExistPost() throws Exception {
        BankId bankId = getBankId();
        ProviderBankInfo providerBankInfo = getProviderBankInfoWithBankId();

        when(providerBankInfoServiceMock.findByBankId(anyLong(), any(BankId.class))).thenReturn(providerBankInfo);

        mockMvc.perform(post("/agents/{agentId}/providerBankInfo/search", AGENT_ID)
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(bankId))
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.operatorInfo.recipientAccount", is("40702810200000078965")))
                .andExpect(jsonPath("$.operatorInfo.inn", is("7213823874")))
                .andExpect(jsonPath("$.operatorInfo.kpp", is("8237487871")))
                .andExpect(jsonPath("$.bankInfo.idBankBaseInfo", is(2)))
                .andExpect(jsonPath("$.bankInfo.name", is("Банк ВТБ 24")))
                .andExpect(jsonPath("$.bankInfo.correspondentAccount", is("30101810100000000718")))
                .andExpect(jsonPath("$.bankInfo.address", is("Санкт-Петербург")))
                .andExpect(jsonPath("$.bankInfo.inn", is("2456254623")))
                .andExpect(jsonPath("$.bankInfo.bik", is("2346544262")))
                .andExpect(jsonPath("$.bankInfo.isClosed", is(false)));

        verify(providerBankInfoServiceMock, times(1)).findByBankId(anyLong(), any(BankId.class));
        verifyNoMoreInteractions(providerBankInfoServiceMock);
    }

    @Test//2+3-
    public void findProviderBankInfoWithAgentIdExistWithBankIdNotExistPost() throws Exception {
        BankId bankId = getBankId();
        when(providerBankInfoServiceMock.findByBankId(anyLong(), any(BankId.class))).thenThrow(new BankIdNotFoundException(""));

        mockMvc.perform(post("/agents/{agentId}/providerBankInfo/search", AGENT_ID)
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(bankId))
        )
                .andExpect(status().isNotFound())
                .andExpect(view().name("error/404"))
                .andExpect(forwardedUrl("/WEB-INF/views/error/404.jsp"));

        verify(providerBankInfoServiceMock, times(1)).findByBankId(anyLong(), any(BankId.class));
        verifyZeroInteractions(providerBankInfoServiceMock);
    }

    @Test//2+3o
    public void findProviderBankInfoWithAgentIdExistWithoutBankIdPost() throws Exception {
        BankId bankId = new BankId();
        ProviderBankInfo providerBankInfo = getProviderBankInfoWithoutBankId();

        when(providerBankInfoServiceMock.findByBankId(anyLong(), any(BankId.class))).thenReturn(providerBankInfo);

        mockMvc.perform(post("/agents/{agentId}/providerBankInfo/search", AGENT_ID)
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(bankId))
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.operatorInfo.recipientAccount", is("40702810200000078965")))
                .andExpect(jsonPath("$.operatorInfo.inn", is("7213823874")))
                .andExpect(jsonPath("$.operatorInfo.kpp", is("8237487871")))
                .andExpect(jsonPath("$.bankInfo.idBankBaseInfo", is(1)))
                .andExpect(jsonPath("$.bankInfo.name", is("Белагропромбанк")))
                .andExpect(jsonPath("$.bankInfo.correspondentAccount", is("30101810100000000716")))
                .andExpect(jsonPath("$.bankInfo.address", is("Минск")))
                .andExpect(jsonPath("$.bankInfo.inn", is("7843878923")))
                .andExpect(jsonPath("$.bankInfo.bik", is("9821387412")))
                .andExpect(jsonPath("$.bankInfo.isClosed", is(true)));

        verify(providerBankInfoServiceMock, times(1)).findByBankId(anyLong(),  any(BankId.class));
        verifyNoMoreInteractions(providerBankInfoServiceMock);
    }

    @Test//2-3+
    public void findProviderBankInfoWithAgentIdNotExistWithBankIdExistPost() throws Exception {
        BankId bankId = getBankId();
        when(providerBankInfoServiceMock.findByBankId(anyLong(), any(BankId.class))).thenThrow(new AgentIdNotFoundException(""));

        mockMvc.perform(post("/agents/{agentId}/providerBankInfo/search", AGENT_ID)
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(bankId))
        )
                .andExpect(status().isNotFound())
                .andExpect(view().name("error/404"))
                .andExpect(forwardedUrl("/WEB-INF/views/error/404.jsp"));

        verify(providerBankInfoServiceMock, times(1)).findByBankId(anyLong(), any(BankId.class));
        verifyZeroInteractions(providerBankInfoServiceMock);
    }

    @Test//2-3-
    public void findProviderBankInfoWithAgentIdNotExistWithBankIdNotExistPost() throws Exception {
        BankId bankId = new BankId();
        when(providerBankInfoServiceMock.findByBankId(anyLong(), any(BankId.class))).thenThrow(new AgentIdNotFoundException(""),new BankIdNotFoundException(""));

        mockMvc.perform(post("/agents/{agentId}/providerBankInfo/search", AGENT_ID)
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(bankId))
        )
                .andExpect(status().isNotFound())
                .andExpect(view().name("error/404"))
                .andExpect(forwardedUrl("/WEB-INF/views/error/404.jsp"));

        verify(providerBankInfoServiceMock, times(1)).findByBankId(anyLong(), any(BankId.class));
        verifyZeroInteractions(providerBankInfoServiceMock);
    }

    @Test//2-3o
    public void findProviderBankInfoWithAgentIdNotExistWithoutBankIdPost() throws Exception {
        BankId bankId = new BankId();
        when(providerBankInfoServiceMock.findByBankId(anyLong(), any(BankId.class))).thenThrow(new AgentIdNotFoundException(""));

        mockMvc.perform(post("/agents/{agentId}/providerBankInfo/search", AGENT_ID)
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(bankId))
        )
                .andExpect(status().isNotFound())
                .andExpect(view().name("error/404"))
                .andExpect(forwardedUrl("/WEB-INF/views/error/404.jsp"));

        verify(providerBankInfoServiceMock, times(1)).findByBankId(anyLong(), any(BankId.class));
        verifyZeroInteractions(providerBankInfoServiceMock);
    }

    @Test//2o
    public void findProviderBankInfoWithAgentIdExistWithSomeStringPost() throws Exception {
        mockMvc.perform(post("/agents/{agentId}/providerBankInfo/search", "some_string"))
                .andExpect(status().isBadRequest());
    }

    @Test//---
    public void findProviderBankInfoWithParamsForAnotherUrl1Post() throws Exception {
        ProviderBankInfo providerBankInfo = getProviderBankInfoWithBankId();

        when(providerBankInfoServiceMock.findByBankId(anyLong(), any(BankId.class))).thenReturn(providerBankInfo);
        mockMvc.perform(post("/agents", AGENT_ID, BANK_ID))
                .andExpect(status().isOk());

        verify(providerBankInfoServiceMock, never()).findByBankId(anyLong(), any(BankId.class));
    }

    @Test//---
    public void findProviderBankInfoWithParamsForAnotherUrl2Post() throws Exception {
        ProviderBankInfo providerBankInfo = getProviderBankInfoWithBankId();

        when(providerBankInfoServiceMock.findByBankId(anyLong(), any(BankId.class))).thenReturn(providerBankInfo);
        mockMvc.perform(post("/agents/providerBankInfo", AGENT_ID, BANK_ID))
                .andExpect(status().isOk());

        verify(providerBankInfoServiceMock, never()).findByBankId(anyLong(), any(BankId.class));
    }

    @Test//---
    public void findProviderBankInfoWithParamsForAnotherUrl3Post() throws Exception {
        ProviderBankInfo providerBankInfo = getProviderBankInfoWithBankId();

        when(providerBankInfoServiceMock.findByBankId(anyLong(), any(BankId.class))).thenReturn(providerBankInfo);
        mockMvc.perform(post("/agents/providerBankInfo/search", AGENT_ID, BANK_ID))
                .andExpect(status().isOk());

        verify(providerBankInfoServiceMock, never()).findByBankId(anyLong(), any(BankId.class));
    }


    //invalid json
    @Test
    public void findProviderBankInfoWithSomeStringPost() throws Exception {
        String bankId = "wrong_json_params";
        ProviderBankInfo providerBankInfo = getProviderBankInfoWithBankId();

        when(providerBankInfoServiceMock.findByBankId(anyLong(), any(BankId.class))).thenReturn(providerBankInfo);

        mockMvc.perform(post("/agents/{agentId}/providerBankInfo/search", AGENT_ID)
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(bankId))
        )
                .andExpect(status().isBadRequest());

        verify(providerBankInfoServiceMock, never()).findByBankId(anyLong(), any(BankId.class));
    }

////////////////////////////////GET
    @Test//2+3+
    public void findProviderBankInfoWithAgentIdExistWithBankIdExistGet() throws Exception {
        ProviderBankInfo providerBankInfo = getProviderBankInfoWithBankId();

        when(providerBankInfoServiceMock.findById(anyLong(), anyLong())).thenReturn(providerBankInfo);

        mockMvc.perform(get("/agents/{agentId}/providerBankInfo", AGENT_ID, BANK_ID))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.operatorInfo.recipientAccount", is("40702810200000078965")))
                .andExpect(jsonPath("$.operatorInfo.inn", is("7213823874")))
                .andExpect(jsonPath("$.operatorInfo.kpp", is("8237487871")))
                .andExpect(jsonPath("$.bankInfo.idBankBaseInfo", is(2)))
                .andExpect(jsonPath("$.bankInfo.name", is("Банк ВТБ 24")))
                .andExpect(jsonPath("$.bankInfo.correspondentAccount", is("30101810100000000718")))
                .andExpect(jsonPath("$.bankInfo.address", is("Санкт-Петербург")))
                .andExpect(jsonPath("$.bankInfo.inn", is("2456254623")))
                .andExpect(jsonPath("$.bankInfo.bik", is("2346544262")))
                .andExpect(jsonPath("$.bankInfo.isClosed", is(false)));

        verify(providerBankInfoServiceMock, times(1)).findById(anyLong(), anyLong());
        verifyNoMoreInteractions(providerBankInfoServiceMock);
    }

    @Test//2+3-
    public void findProviderBankInfoWithAgentIdExistWithBankIdNotExistGet() throws Exception {
        when(providerBankInfoServiceMock.findById(anyLong(), anyLong())).thenThrow(new BankIdNotFoundException(""));

        mockMvc.perform(get("/agents/{agentId}/providerBankInfo", AGENT_ID, BANK_ID))
                .andExpect(status().isNotFound())
                .andExpect(view().name("error/404"))
                .andExpect(forwardedUrl("/WEB-INF/views/error/404.jsp"));

        verify(providerBankInfoServiceMock, times(1)).findById(anyLong(), anyLong());
        verifyZeroInteractions(providerBankInfoServiceMock);
    }

    @Test//2+3o
    public void findProviderBankInfoWithAgentIdExistWithoutBankIdGet() throws Exception {
        ProviderBankInfo providerBankInfo = getProviderBankInfoWithoutBankId();

        when(providerBankInfoServiceMock.findById(AGENT_ID, null)).thenReturn(providerBankInfo);

        mockMvc.perform(get("/agents/{agentId}/providerBankInfo", AGENT_ID, null))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.operatorInfo.recipientAccount", is("40702810200000078965")))
                .andExpect(jsonPath("$.operatorInfo.inn", is("7213823874")))
                .andExpect(jsonPath("$.operatorInfo.kpp", is("8237487871")))
                .andExpect(jsonPath("$.bankInfo.idBankBaseInfo", is(1)))
                .andExpect(jsonPath("$.bankInfo.name", is("Белагропромбанк")))
                .andExpect(jsonPath("$.bankInfo.correspondentAccount", is("30101810100000000716")))
                .andExpect(jsonPath("$.bankInfo.address", is("Минск")))
                .andExpect(jsonPath("$.bankInfo.inn", is("7843878923")))
                .andExpect(jsonPath("$.bankInfo.bik", is("9821387412")))
                .andExpect(jsonPath("$.bankInfo.isClosed", is(true)));

        verify(providerBankInfoServiceMock, times(1)).findById(AGENT_ID, null);
        verifyNoMoreInteractions(providerBankInfoServiceMock);
    }

    @Test//2-3+
    public void findProviderBankInfoWithAgentIdNotExistWithBankIdExistGet() throws Exception {
        when(providerBankInfoServiceMock.findById(anyLong(), anyLong())).thenThrow(new AgentIdNotFoundException(""));

        mockMvc.perform(get("/agents/{agentId}/providerBankInfo", AGENT_ID, BANK_ID))
                .andExpect(status().isNotFound())
                .andExpect(view().name("error/404"))
                .andExpect(forwardedUrl("/WEB-INF/views/error/404.jsp"));

        verify(providerBankInfoServiceMock, times(1)).findById(anyLong(), anyLong());
        verifyZeroInteractions(providerBankInfoServiceMock);
    }

    @Test//2-3-
    public void findProviderBankInfoWithAgentIdNotExistWithBankIdNotExistGet() throws Exception {
        when(providerBankInfoServiceMock.findById(anyLong(), anyLong())).thenThrow(new AgentIdNotFoundException(""),new BankIdNotFoundException(""));

        mockMvc.perform(get("/agents/{agentId}/providerBankInfo", AGENT_ID, BANK_ID))
                .andExpect(status().isNotFound())
                .andExpect(view().name("error/404"))
                .andExpect(forwardedUrl("/WEB-INF/views/error/404.jsp"));

        verify(providerBankInfoServiceMock, times(1)).findById(anyLong(), anyLong());
        verifyZeroInteractions(providerBankInfoServiceMock);
    }

    @Test//2-3o
    public void findProviderBankInfoWithAgentIdNotExistWithoutBankIdGet() throws Exception {
        when(providerBankInfoServiceMock.findById(AGENT_ID, null)).thenThrow(new AgentIdNotFoundException(""));

        mockMvc.perform(get("/agents/{agentId}/providerBankInfo", AGENT_ID, null))
                .andExpect(status().isNotFound())
                .andExpect(view().name("error/404"))
                .andExpect(forwardedUrl("/WEB-INF/views/error/404.jsp"));

        verify(providerBankInfoServiceMock, times(1)).findById(AGENT_ID, null);
        verifyZeroInteractions(providerBankInfoServiceMock);
    }

    @Test//2o
    public void findProviderBankInfoWithSomeStringGet() throws Exception {
        mockMvc.perform(get("/agents/{agentId}/providerBankInfo", "wrong_string_params"))
                .andExpect(status().isBadRequest());
    }

    @Test//---
    public void findProviderBankInfoWithParamsForAnotherUrl1Get() throws Exception {
        ProviderBankInfo providerBankInfo = getProviderBankInfoWithBankId();

        when(providerBankInfoServiceMock.findById(anyLong(), anyLong())).thenReturn(providerBankInfo);
        mockMvc.perform(get("/agents", AGENT_ID, BANK_ID))
                .andExpect(status().isOk());
        verify(providerBankInfoServiceMock, never()).findById(anyLong(), anyLong());
    }

    @Test//---
    public void findProviderBankInfoWithParamsForAnotherUrl2Get() throws Exception {
        ProviderBankInfo providerBankInfo = getProviderBankInfoWithBankId();

        when(providerBankInfoServiceMock.findById(anyLong(), anyLong())).thenReturn(providerBankInfo);
        mockMvc.perform(get("/agents/providerBankInfo", AGENT_ID, BANK_ID))
                .andExpect(status().isOk());
        verify(providerBankInfoServiceMock, never()).findById(anyLong(), anyLong());
    }


    private BankId getBankId() {
        BankId bankId = new BankIdBuilder()
                .bankId(BANK_ID)
                .build();

        return bankId;
    }

    private ProviderInfo getProviderInfo() {
        ProviderInfo providerInfo = new ProviderInfoBuilder()
                .idProviderInfo(1L)
                .recipientAccount("40702810200000078965")
                .inn("7213823874")
                .kpp("8237487871")
                .build();

        return providerInfo;
    }

    private BankBaseInfo getBankBaseInfo1() {
        BankBaseInfo bankBaseInfo = new BankBaseInfoBuilder()
                .idBankBaseInfo(1L)
                .name("Белагропромбанк")
                .correspondentAccount("30101810100000000716")
                .address("Минск")
                .inn("7843878923")
                .bik("9821387412")
                .isClosed(true)
                .build();

        return bankBaseInfo;
    }

    private BankBaseInfo getBankBaseInfo2() {
        BankBaseInfo bankBaseInfo = new BankBaseInfoBuilder()
                .idBankBaseInfo(BANK_ID)
                .name("Банк ВТБ 24")
                .correspondentAccount("30101810100000000718")
                .address("Санкт-Петербург")
                .inn("2456254623")
                .bik("2346544262")
                .isClosed(false)
                .build();

        return bankBaseInfo;
    }

    private ProviderBankInfo getProviderBankInfoWithoutBankId() {
        ProviderBankInfo providerBankInfo = new ProviderBankInfoBuilder()
                .idAgent(1L)
                .operatorInfo(getProviderInfo())
                .bankInfo(getBankBaseInfo1())
                .build();

        return providerBankInfo;
    }

    private ProviderBankInfo getProviderBankInfoWithBankId() {
        ProviderBankInfo providerBankInfo = new ProviderBankInfoBuilder()
                .idAgent(1L)
                .operatorInfo(getProviderInfo())
                .bankInfo(getBankBaseInfo2())
                .build();

        return providerBankInfo;
    }

}